import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage';
import {AppModule} from './app/app.module';
import {environment} from './environments/environment';
// import * as conf from './config/config.json';

if (environment.production) {
  enableProdMode();
}
// // Amplify Configuration
// import Auth from '@aws-amplify/auth';
// import Storage from '@aws-amplify/storage';
//
// Storage.configure(conf.amplify.Storage);
// Auth.configure(conf.amplify.Auth);
// // End Amplify Configuration
fetch('./config/config.json').then(res => {
  res.json().then((res)=>{
    console.log(res.amplify.Storage);
    Storage.configure(res.amplify.Storage);
    Auth.configure(res.amplify.Auth);
  })

}) .catch((e) => console.log(e))


platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
