import { NgModule } from '@angular/core';

import { NotConnectedRoutingModule } from './not-connected-routing.module';
import {LoginComponent} from "./components/login/login.component";
import {RegisterComponent} from "./components/register/register.component";
import { HomeComponent } from './components/home/home.component';
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    HomeComponent
  ],
  imports: [
    SharedModule,
    NotConnectedRoutingModule,

  ]
})
export class NotConnectedModule { }
