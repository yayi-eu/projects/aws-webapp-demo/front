import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { map, tap, catchError } from 'rxjs/operators';
import Amplify, { Auth } from 'aws-amplify';

import {Store} from "@ngrx/store";
import {BehaviorSubject, Observable, of} from "rxjs";
import {AppState} from "../../shared/store/app.states";
import {fromPromise} from "rxjs/internal-compatibility";
import {StoreToken} from "../../shared/store/actions/auth.actions";
import * as conf from '../../../../config/config.json'

@Injectable()
export class AuthService {

    public loggedIn: BehaviorSubject<boolean>;

    constructor(
        private router: Router,
        private store: Store<AppState>
    ) {
        Amplify.configure(conf.amplify);
        this.loggedIn = new BehaviorSubject<boolean>(false);
    }

    /** signup */
    public signUp(email, password , name): Observable<any> {
        let params = {
            email,
            password,
            attributes: {
                name: name
            }}
        return fromPromise(Auth.signUp(email, password));
    }


    /** confirm code */
    public confirmSignUp(email, code): Observable<any> {
        return fromPromise(Auth.confirmSignUp(email, code));
    }

    /** signin */
    public signIn(email, password ): Observable<any> {
        return fromPromise(Auth.signIn(email, password))
            .pipe(
                tap(() => {
                    this.loggedIn.next(true);
                    Auth.currentSession().then( res =>{
                        let payload = {
                            rememberMe: false,
                            token: res.getIdToken().getJwtToken()
                        };

                        this.store.dispatch(new StoreToken(payload));
                    });
                }),

            );
    }

    /** get authenticat state */
    public isAuthenticated(): Observable<boolean> {
        return fromPromise(Auth.currentAuthenticatedUser())
            .pipe(
                map(result => {
                    this.loggedIn.next(true);
                    return true;
                }),
                catchError(error => {
                    this.loggedIn.next(false);
                    return of(false);
                })
            );
    }

    /** signout */
    public signOut() {
        fromPromise(Auth.signOut())
            .subscribe(
                result => {
                    this.loggedIn.next(false);
                    sessionStorage.removeItem('token');
                    localStorage.removeItem('token');
                    this.router.navigate(['/login']);
                },
                error => console.log(error)
            );
    }
    public saveToken(token: string, rememberMe: boolean) {
        if (rememberMe) {
            localStorage.setItem('token', token);
        } else {
            sessionStorage.setItem('token', token);
        }
    }

    public getToken(): string {
        let token = localStorage.getItem('token');
        if (token == null) {
            token = sessionStorage.getItem('token');
        }
        return token;
    }


}
