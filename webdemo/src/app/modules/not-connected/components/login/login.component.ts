import {Component, Input, OnDestroy, OnInit, Output} from '@angular/core';
import Auth from '@aws-amplify/auth';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

import {Store} from "@ngrx/store";
import {AppState} from "../../../shared/store/app.states";
import {LogIn} from "../../../shared/store/actions/auth.actions";

@Component({
    selector: 'app-dashboard',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    form: FormGroup;
    private rememberMe: boolean = false;

    constructor(private route: Router , private toastr: ToastrService,
                private store: Store<AppState>) {
    }

    ngOnInit() {
        this.resetForm();
    }

    ngOnDestroy(): void {
    }

    onSubmit() {
        const payload = {
            email: this.form.get('username').value,
            password: this.form.get('password').value
        };

        this.store.dispatch(new LogIn(payload));
    }
    private resetForm() {
        this.form = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        });
    }

}
