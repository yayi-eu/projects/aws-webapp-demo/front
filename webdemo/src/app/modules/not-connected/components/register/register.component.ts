import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Store} from "@ngrx/store";
import {AppState} from "../../../shared/store/app.states";
import {SignUp} from "../../../shared/store/actions/auth.actions";




@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    form: FormGroup;

    constructor(private store: Store<AppState>) {
    }

    ngOnInit() {

        this.resetForm();
    }

    onSubmit() {

        const payload = {
            email: this.form.get('email').value,
            password: this.form.get('password').value,
            name:  this.form.get('username').value
        };
        this.store.dispatch(new SignUp(payload));
    }

    private resetForm() {
        this.form = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
            repass: new FormControl('', Validators.required),
            email: new FormControl('', Validators.required)
        });
    }
}
