export class Comment {

  constructor() {
  }
  private _name: string;

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }
}
