import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) {
  }

  get() {
    return this.http.get('/comments');
  }

  delete(id: any) {
    return this.http.delete('/comments/' + id);
  }

  post(payload: any) {
    return this.http.post('/comments', payload);
  }

  put(payload: any) {
    return this.http.put('/comments/' + payload.id, {payload});
  }


}
