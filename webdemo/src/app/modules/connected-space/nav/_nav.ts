import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
    {
        name: 'Dashboard',
        url: '/my-space',
        icon: 'icon-speedometer',
        badge: {
            variant: 'info',
            text: 'NEW'
        }
    },
    {
        title: true,
        name: 'Menu'
    },
    {
        name: 'home',
        url: '/my-space',
        icon: 'icon-user'
    },
    {
        name: 'upload',
        url: '/my-space/file',
        icon: 'icon-user'
    },

];
