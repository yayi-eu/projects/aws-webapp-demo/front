import { Component, OnInit } from '@angular/core';
import {CommentsService} from "../../services/comments.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public comments: any;

  constructor(private commentService: CommentsService) { }

  ngOnInit(): void {
    this.getComments();
  }
  getComments(){
    this.commentService.get().subscribe( res =>{
      this.comments = res ;
    })
  }
}
