import {Component, OnInit} from '@angular/core';
import {Storage} from 'aws-amplify';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

  uploadPublic() {
    Storage.put('test.txt', 'Hello')
      .then(result => console.log(result)) // {key: "test.txt"}
      .catch(err => console.log(err));
  }

  uploadProtected() {
    Storage.put('test-protected.txt', 'Hello', {
      level: 'protected'
    })
      .then(result => console.log(result)) // {key: "test.txt"}
      .catch(err => console.log(err));
  }

  uploadPrivate() {
    Storage.put('test-private.txt', 'Hello', {
      level: 'private'
    })
      .then(result => console.log(result)) // {key: "test.txt"}
      .catch(err => console.log(err));
  }

}
