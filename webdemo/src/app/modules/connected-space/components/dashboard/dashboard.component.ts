import { Component, OnInit } from '@angular/core';
import {AppState, selectAuthState} from "../../../shared/store/app.states";
import { navItems } from '../../nav/_nav';
import {Store} from "@ngrx/store";
import {pluck} from "rxjs/operators";
import {Observable} from "rxjs";
import {Logout} from "../../../shared/store/actions/auth.actions";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = navItems;
  public state: Observable<any>;
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  constructor(private store: Store<AppState>) {
    this.state = this.store.select(selectAuthState);
  }

  ngOnInit(): void {
    this.state.pipe(pluck('user')).subscribe( (user) =>{
      if(user!==null){
        console.table(user);
      }
    })
  }
  logout(): void {
    this.store.dispatch(new Logout());
  }

}
