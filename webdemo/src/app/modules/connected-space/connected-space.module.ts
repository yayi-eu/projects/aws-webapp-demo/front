import { NgModule } from '@angular/core';

import { ConnectedSpaceRoutingModule } from './connected-space-routing.module';
import {SharedModule} from "../shared/shared.module";
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import {AppAsideModule, AppBreadcrumbModule, AppFooterModule, AppHeaderModule, AppSidebarModule} from "@coreui/angular";
import {BsDropdownModule} from "ngx-bootstrap/dropdown";
import {TabsModule} from "ngx-bootstrap/tabs";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {AppModule} from "../../app.module";
import { FileComponent } from './components/file/file.component';


@NgModule({
  declarations: [DashboardComponent, HomeComponent, FileComponent],
  imports: [
    SharedModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ConnectedSpaceRoutingModule,
  ],

})
export class ConnectedSpaceModule { }
