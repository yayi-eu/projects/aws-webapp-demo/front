
import {All, AuthActionTypes, StoreToken} from "../actions/auth.actions";
import Amplify, { Auth } from 'aws-amplify';
import {User} from "../../../not-connected/models/user";


export interface State {
    // is a user authenticated?
    isAuthenticated: boolean;
    // if authenticated, there should be a user object
    user: User | null;
    session: any;
    // error message
    errorMessage: string | null;
}
export const initialState: State = {
    isAuthenticated: false,
    user: null,
    session: null,
    errorMessage: null
};
export function reducer(state = initialState, action: All): State {
    switch (action.type) {
        case AuthActionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                user: {
                    email: action.payload.email,
                },
                errorMessage: null
            };
        }
        case AuthActionTypes.LOGIN_FAILURE: {
            return {
                ...state,
                errorMessage: 'Incorrect email and/or password.'
            };
        }
        case AuthActionTypes.SIGN_UP_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                user: {
                    email: action.payload.email
                },
                errorMessage: null
            };
        }
        case AuthActionTypes.SIGN_UP_FAILURE: {
            return {
                ...state,
                errorMessage: 'That email is already in use.'
            };
        }
        case AuthActionTypes.STORE_TOKEN: {
            return {
                ...state,
                isAuthenticated: true,
                session: {
                    session: action.payload
                },
                errorMessage: 'That email is already in use.'
            };
        }
        case AuthActionTypes.LOGOUT: {
            return {
                ...state,
                isAuthenticated: false,
            };
        }
        default: {
            return state;
        }
    }
}
