import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Router} from '@angular/router';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {
    AuthActionTypes,
    LogIn, LogInSuccess, LogInFailure, SignUp, SignUpSuccess, SignUpFailure,
} from '../actions/auth.actions';
import {AuthService} from "../../../not-connected/services/auth.service";




@Injectable()
export class AuthEffects {

    constructor(
        private actions: Actions,
        private authService: AuthService,
        private router: Router,
    ) {
    }

    @Effect()
    LogIn: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN),
        map((action: LogIn) => action.payload)
        ,switchMap(payload => {
            return this.authService.signIn(payload.email, payload.password)
                .map((user) => {
                    return new LogInSuccess({email: payload.email});
                })
                .catch((error) => {
                    return Observable.of(new LogInFailure({error: error}));
                });
        }));
    @Effect({ dispatch: false })
    LogInSuccess: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_SUCCESS),
        tap((user) => {
            this.router.navigate(['/my-space']);
        })
    );
    @Effect({ dispatch: false })
    LogInFailure: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_FAILURE),
        tap(()=>{
            this.router.navigate(['/auth/login']);
        })
    );
    @Effect()
    SignUp: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.SIGN_UP),
        map((action: SignUp) => action.payload),
        switchMap(payload => {
            return this.authService.signUp(payload.email, payload.password , payload.name)
                .map((user) => {
                    return new SignUpSuccess({email: payload.email});
                }).catch((error) => {
                    return Observable.of(new SignUpFailure({ error: error }));
                })
        }));

    @Effect({ dispatch: false })
    SignUpSuccess: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.SIGN_UP_SUCCESS),
        tap((user) => {
            this.router.navigate(['/auth/login']);
        })
    );

    @Effect({ dispatch: false })
    SignUpFailure: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.SIGN_UP_FAILURE)
    );
    @Effect({ dispatch: false })
    StoreToken: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.STORE_TOKEN),
        tap((res) =>{
            let token = res.payload.token;
            let bool = res.payload.rememberMe;
            this.authService.saveToken(token,bool);
        })
    );

    @Effect({ dispatch: false })
    Logout: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGOUT),
        tap((res) =>{
            this.authService.signOut();
        })
    );

}
