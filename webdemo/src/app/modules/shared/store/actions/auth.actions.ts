import { Action } from '@ngrx/store';


export enum AuthActionTypes {
    LOGIN = '[Auth] Login',
    LOGIN_SUCCESS = '[Auth] Login Success',
    LOGIN_FAILURE = '[Auth] Login Failure',
    SIGN_UP = '[Auth] Signup',
    SIGN_UP_SUCCESS = '[Auth] Signup Success',
    SIGN_UP_FAILURE = '[Auth] Signup Failure',
    STORE_TOKEN = '[Auth] Store Token',
    LOGOUT = '[Auth] Logout',

}

export class LogIn implements Action {
    readonly type = AuthActionTypes.LOGIN;
    constructor(public payload: any) {}
}

export class LogInSuccess implements Action {
    readonly type = AuthActionTypes.LOGIN_SUCCESS;
    constructor(public payload: any) {}
}

export class LogInFailure implements Action {
    readonly type = AuthActionTypes.LOGIN_FAILURE;
    constructor(public payload: any) {}
}
export class SignUp implements Action {
    readonly type = AuthActionTypes.SIGN_UP;
    constructor(public payload: any) {}
}
export class SignUpSuccess implements Action {
    readonly type = AuthActionTypes.SIGN_UP_SUCCESS;
    constructor(public payload: any) {}
}
export class SignUpFailure implements Action {
    readonly type = AuthActionTypes.SIGN_UP_FAILURE;
    constructor(public payload: any) {}
}
export class StoreToken implements Action {
    readonly type = AuthActionTypes.STORE_TOKEN;
    constructor(public payload: any) {}
}

export class Logout implements Action {
    readonly type = AuthActionTypes.LOGOUT;
    constructor() {}
}

export type All =
    | LogIn
    | LogInSuccess
    | LogInFailure
    | SignUp
    | SignUpSuccess
    | SignUpFailure
    | StoreToken
    | Logout;