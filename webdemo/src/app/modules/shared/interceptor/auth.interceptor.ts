import {Injectable} from "@angular/core";
import {HTTP_INTERCEPTORS, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  // public BASE_URL = conf.apiUrl;
  public BASE_URL: any;
  constructor() {
  }
  getBaseUrl() {
    fetch('../../../../config/config.json').then(res => {
      res.json().then((res) => {
        localStorage.setItem('BASE_URL', res.apiUrl);
      });
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.getBaseUrl();
    this.BASE_URL = localStorage.getItem('BASE_URL');
    let token = null
    let authReq = req;
    if (sessionStorage.getItem('token')) {
      token = sessionStorage.getItem('token');
    } else {
      token = localStorage.getItem('token');
    }
    if (token != null) {
      authReq = req.clone({
        url: this.BASE_URL + req.url,
        headers: req.headers.set(TOKEN_HEADER_KEY, token)
      });
    } else {
      authReq = req.clone({url: this.BASE_URL + req.url});
    }
    return next.handle(authReq);
  }
}

export const httpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
];
