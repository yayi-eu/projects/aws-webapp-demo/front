import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'auth', loadChildren: () => import('./modules/not-connected/not-connected.module').then(m => m.NotConnectedModule)},
  {path: 'my-space', loadChildren: () => import('./modules/connected-space/connected-space.module').then(m => m.ConnectedSpaceModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
