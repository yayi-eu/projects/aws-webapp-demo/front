import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ToastrModule} from "ngx-toastr";
import {EffectsModule} from "@ngrx/effects";
import {AuthEffects} from "./modules/shared/store/effects/auth.effects";
import {StoreModule} from "@ngrx/store";
import {reducers} from "./modules/shared/store/app.states";
import {AuthService} from "./modules/not-connected/services/auth.service";
import {AuthInterceptor, httpInterceptorProviders} from "./modules/shared/interceptor/auth.interceptor";
import {AuthGuard} from "./modules/shared/guard/auth.guard";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    EffectsModule.forRoot([AuthEffects]),
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      }
    }),

  ],
  providers: [AuthService,
    AuthInterceptor,
    httpInterceptorProviders,
    AuthGuard,],
  bootstrap: [AppComponent]
})
export class AppModule {
}
